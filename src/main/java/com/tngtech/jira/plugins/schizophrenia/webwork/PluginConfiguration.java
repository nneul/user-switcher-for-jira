package com.tngtech.jira.plugins.schizophrenia.webwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Strings;
import com.tngtech.jira.plugins.schizophrenia.ao.SwitchUserConfig;
import com.tngtech.jira.plugins.schizophrenia.component.SwitchUserConfigService;

@WebSudoRequired
public class PluginConfiguration extends JiraWebActionSupport {

    private SwitchUserConfig switchUserConfig;
    private String successMsg;

    @Autowired
    private SwitchUserConfigService switchUserConfigService;

    @Autowired
    private GroupManager groupManager;

    @Autowired
    JiraAuthenticationContext jiraAuthenticationContext;

    @RequiresXsrfCheck
    @Override
    public String execute() throws Exception {
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        successMsg = null;
        if (getHttpRequest().getParameter("save") != null) {
            String assignableGroup = getAssignableGroupFromRequest();
            if (!Strings.isNullOrEmpty(assignableGroup) && !groupManager.groupExists(assignableGroup)) {
                addErrorMessage(String.format(i18nHelper.getText(
                        "schizophrenia.configuration.error.groupDoesNotExists", assignableGroup)));
            }

            String groupAllowedToSwitch = getGroupAllowedToSwitchFromRequest();
            if (!Strings.isNullOrEmpty(groupAllowedToSwitch) && !groupManager.groupExists(groupAllowedToSwitch)) {
                addErrorMessage(String.format(i18nHelper.getText(
                        "schizophrenia.configuration.error.groupDoesNotExists", groupAllowedToSwitch)));
            }

            if (getHasErrorMessages()) {
                return INPUT;
            }

            switchUserConfigService.storeConfiguration(getGroupAllowedToSwitchFromRequest(),
                    getAssignableGroupFromRequest());
            successMsg = "Saved configuration!";
        }

        switchUserConfig = switchUserConfigService.getValidConfiguration();
        return SUCCESS;
    }

    private String getGroupAllowedToSwitchFromRequest() {
        String groupAllowedToSwitch = getHttpRequest().getParameter("groupAllowedToSwitch");
        return groupAllowedToSwitch;
    }

    private String getAssignableGroupFromRequest() {
        String assignableGroup = getHttpRequest().getParameter("assignableGroup");
        return assignableGroup;
    }

    public SwitchUserConfig getSwitchUserConfig() {
        return switchUserConfig;
    }

    public String getSuccessMessage() {
        return successMsg;
    }
}
