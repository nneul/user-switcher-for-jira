package com.tngtech.jira.plugins.schizophrenia.rest;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.tngtech.jira.plugins.schizophrenia.rest.model.User;

@XmlRootElement(name = "userlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class UsernameListResponse {

    @XmlElement
    private Collection<User> users;

    public UsernameListResponse(Collection<User> users) {
        this.users = users;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

}
