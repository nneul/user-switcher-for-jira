package com.tngtech.jira.plugins.schizophrenia.ao;

import net.java.ao.RawEntity;
import net.java.ao.schema.PrimaryKey;

public interface SwitchUserConfig extends RawEntity<Integer> {

    @PrimaryKey
    Integer getIdentifier();
    
    void setIdentifier(Integer identifier);
    
	String getGroupAllowedToSwitch();

	void setGroupAllowedToSwitch(String groupAllowedToSwitch);

	String getAssignableGroup();

	void setAssignableGroup(String assignableGroup);

}
