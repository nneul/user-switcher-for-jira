package com.tngtech.jira.plugins.schizophrenia.exception;

import com.atlassian.jira.user.ApplicationUser;

public class UserNotAllowedToSwitchException extends Exception {

    public UserNotAllowedToSwitchException(ApplicationUser user) {
        super(String.format("User '%s' has no permission to switch the user.", user.getUsername()));
    }

}
