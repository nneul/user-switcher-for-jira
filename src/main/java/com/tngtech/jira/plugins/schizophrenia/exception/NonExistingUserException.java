package com.tngtech.jira.plugins.schizophrenia.exception;


public class NonExistingUserException extends Exception {

	public NonExistingUserException(String username) {
		super(String.format("No user found for '%s'", username));
	}

}
