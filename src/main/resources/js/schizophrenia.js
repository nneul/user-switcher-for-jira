AJS.namespace('Schizophrenia', window, Class.extend({
	restApiUrl : null,
	
	init: function(baseUrl) {
		this.restApiUrl = baseUrl + '/rest/schizophrenia/1.0';
	},
		
	/**
	 * Sorts the user in an array by the locale display name.
	 * 
	 * @param users an array of users where the user is an object with an attribute "displayName" at least
	 * @param firstUser a name of a user that should be the first name after sorting
	 */
	sortUsers: function(users, firstUser) {
		users.sort(function(a, b) {
			if(firstUser && firstUser === a.username) {
				return -1;
			} else if(firstUser && firstUser == b.username) { 
				return 1;
			} else if(!a || !a.displayName) {
				return -1;
			} else if(!b || !b.displayName) {
				return 1;
			} else {
				return a.displayName.toLocaleLowerCase().localeCompare(b.displayName.toLocaleLowerCase());
			}
		});
	},
			
	/**
	 * Switch to the given username. Shows an error if switching fails. Only switches if current user is 
	 * different to given user.
	 * If this is the first switch, the original user is stored and can be retrieved via getOriginalUser.
	 * 
	 * @param username the username of a user to switch to
	 */
	switchToUser: function(username) {
		if(username !== AJS.params.loggedInUser) {
			AJS.$.post(this.restApiUrl + '/switchuser/switch/' + username).success(function() {
				window.location.reload(true);
			}).error(function() {
				alert(AJS.I18n.getText("schizophrenia.switchuser.error") + " " + username);
			});
		}
	},
	
	getRealUser: function(callback, errorCallback) {
		AJS.$.getJSON(this.restApiUrl + '/switchuser/realuser', function(response) {
			callback(response);
		}).error(errorCallback);
	},
	
	getUserInfo: function(username, callback, errorCallback) {
		AJS.$.getJSON(this.restApiUrl + '/switchuser/userinformation/' + username, function(response) {
			callback(response);
		}).error(errorCallback);
	},
	
	mayUserSwitch: function(callback) {
		AJS.$.getJSON(this.restApiUrl + '/switchuser/mayuserswitch', function(response) {
			callback(response);
		}).error(function() {
			callback({
				mayUserSwitch: false
			});
		});
	}
}));

